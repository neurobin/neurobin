<nav class="navbar" id="navbar">
			<div class="container-fluid" id="bar">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand"><img alt="neurobin logo" id="logo" src="img/neurobin400.png" /></a>
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav" id="menu">
						<li class="active">
							<a href="" target="_parent">Home</a>
						</li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">Softwares<span class="caret-down"></span></a>
							<ul class="dropdown-menu">
								<li>
									<a class="menu-item" href="linsofts.php">Linux</a>

								</li>
								<li>
									<a class="menu-item" href="winsofts.php">Windows</a>
								</li>
								<li>
									<a class="menu-item" href="andapps.php">Android</a>
								</li>
							</ul>
						</li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">Tutorials<span class="caret-down"></span></a>
							<ul class="dropdown-menu">
								<li>
									<a class="menu-item" href="#" target="_parent">Linux<span class="caret-right"></span></a>
									<ul>

										<li>
											<a class="menu-item disabled-link" href="#">Generic</a>
										</li>
										<li>
											<a class="menu-item" href="Tutorials/linuxtutorials.php">Ubuntu</a>
										</li>
									</ul>
								</li>
								<li class="disabled-link">
									<a class="menu-item disabled-link" >Programming<span class="caret-right"></span></a>
									<ul class="programming">
										<li>
											<a class="menu-item disabled-link" href="#">C</a>
										</li>
										<li>
											<a class="menu-item disabled-link" href="#">C++</a>
										</li>
										<li>
											<a class="menu-item disabled-link" href="#">SQL</a>
										</li>
										<li>
											<a class="menu-item disabled-link" href="#">PHP</a>
										</li>
										<li>
											<a class="menu-item disabled-link" href="#">CSS</a>
										</li>
										<li>
											<a class="menu-item disabled-link" href="#">Java</a>
										</li>
										<li>
											<a class="menu-item disabled-link" href="#">HTML</a>
										</li>
										<li>
											<a class="menu-item disabled-link" href="#">Matlab</a>
										</li>
										<li>
											<a class="menu-item disabled-link" href="#">JavaScript</a>
										</li>
										<li>
											<a class="menu-item disabled-link" href="#">Shell Scripting</a>
										</li>
									</ul>

								</li>

								<li>
									<a class="menu-item disabled-link" href="#">Android</a>
								</li>
							</ul>

						</li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">Places<span class="caret-down"></span></a>
							<ul class="dropdown-menu">
								<li>
									<a class="menu-item" href="http://github.com/neurobin"><i class="fa fa-github"></i> Github</a>

								</li>
								<li>
									<a class="menu-item" href="http://bitbucket.org/neurobin"><i class="fa fa-bitbucket"></i> Bitbucket</a>
								</li>

							</ul>
						</li>
						<li>
							<a class="menu-item disabled-link" href="downloads.php">Downloads</a>
						</li>

					</ul>
					<div class="clock">
						<ul class="nav navbar-nav navbar-right">

							<li class="clock-item" >
								<p id="clockTime"></p><p id="clockDate"></p>
							</li>

						</ul>
					</div>
				</div>
			</div>
		</nav>